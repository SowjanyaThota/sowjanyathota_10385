import {Hero} from './hero';

export const Heroes : Hero[] = [
    {id:11,name:'John'},
    {id:12,name:'tom'},
    {id:13,name:'Jerry'},
    {id:14,name:'Doremon'},
    {id:15,name:'Barbie'},
    {id:16,name:'Max'},
    {id:17,name:'Jack'},
    {id:18,name:'Oswald'},
    {id:19,name:'Mickey'},
    {id:20,name:'Patlu'} 
];