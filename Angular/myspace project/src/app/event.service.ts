import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }
  getData() {
    return this.http.get('http://192.168.150.66:8080/myspace/displayEvents')
  }
}
