import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  id: any;
  private details:any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    debugger;
    console.log(localStorage.getItem('userId'))
    this.id = localStorage.getItem('userId');
    this.http.get('http://192.168.150.66:8080/myspace/viewprofile/'+ this.id)
      .subscribe((res) => {
        debugger;
        console.log(res);
       this.details  = res;
      })

  }


}
