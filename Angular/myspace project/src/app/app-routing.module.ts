import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './events/events.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { PostDataFakeComponent } from './post-data-fake/post-data-fake.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';

const routes: Routes = [
  {path:'dashboard',component:DashboardComponent},
  {path:'events',component: EventsComponent},
  {path:'profile',component: ProfileComponent},
  {path:'postData',component:PostDataFakeComponent},
  {path:'updateProfile',component:UpdateProfileComponent},

  {path:'',component:LoginComponent},
  // {path:'**',component:PageNotFoundComponent},
  // {path:'',redirectTo:'/login',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
