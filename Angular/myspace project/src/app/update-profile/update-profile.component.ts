import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {
  id: any;
  private details:any;
  constructor(private http: HttpClient,private router:Router) { }
  res: any;
  ngOnInit() {
    debugger;
    console.log(localStorage.getItem('userId'))
    this.id = localStorage.getItem('userId');
    this.http.get('http://192.168.150.66:8080/myspace/viewprofile/'+ this.id)
      .subscribe((res) => {
        debugger;
        console.log(res);
       this.details  = res;
      })
  }
  onSubmit(data) {
    debugger;
    console.log(data)
    this.http.put(`http://192.168.150.66:8080/myspace/updateprofile/${localStorage.getItem('userId')}`, data)
      .subscribe((res) => {
        console.log(res);
        this.router.navigate[('/profile')]
        debugger;
      })
  }

}
