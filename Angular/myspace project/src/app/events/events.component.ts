import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  private events = [];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('http://192.168.150.66:8080/myspace/displayEvents').subscribe((res:any[])=> {
      console.log(res);
      this.events =res;
    })
  }
  doDelete(eventId):void {
    this.http.delete('http://192.168.150.66:8080/myspace/deleteEvents/'+eventId)
    .subscribe()
  }
}
