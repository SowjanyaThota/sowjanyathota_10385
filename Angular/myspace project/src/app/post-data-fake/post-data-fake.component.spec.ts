import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostDataFakeComponent } from './post-data-fake.component';

describe('PostDataFakeComponent', () => {
  let component: PostDataFakeComponent;
  let fixture: ComponentFixture<PostDataFakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostDataFakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostDataFakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
