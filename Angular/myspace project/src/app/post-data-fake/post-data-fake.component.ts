import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post-data-fake',
  templateUrl: './post-data-fake.component.html',
  styleUrls: ['./post-data-fake.component.css']
})
export class PostDataFakeComponent implements OnInit {
data : any;
  constructor(private http:HttpClient) { }

  ngOnInit() {
  }
onSubmit(data) {
  console.log(data);
this.http.post('https://jsonplaceholder.typicode.com/posts',data)
.subscribe((res:any)=> {
  console.log(res);
})
}
}
