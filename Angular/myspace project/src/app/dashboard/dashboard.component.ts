import { Component, OnInit, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(private http: HttpClient) { }
  namesearch: any;
  search:string;
  details : any;

  ngOnInit() {
  }
  onSearch() {
    this.http.get('http://192.168.150.66:8080/myspace/searchEmployeeByName/'+this.search)
   .subscribe((word:any)=> {
   this.details  = word;
   })

  }
 
}