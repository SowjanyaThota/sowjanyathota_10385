import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  doLogin() {
    if (localStorage.getItem('token') != null) {
      var result = window.confirm("Do You want To logout");
      if (result === true) {
        localStorage.removeItem("token");
        return (this.router.navigate[("")])
      }
      else {
        return (this.router.navigate[("/dashboard")])
      }
    }
  }
}
