import { Component, OnInit } from '@angular/core';
import { TestService } from 'src/app/test.service';

@Component({
  selector: 'app-view-component',
  templateUrl: './view-component.component.html',
  styleUrls: ['./view-component.component.css']
})
export class ViewComponentComponent implements OnInit {

  todayDate:any;
  constructor( private svc : TestService,private dateService:TestService) { 
  svc.printOnConsole("i got service in child");
  }
  ngOnInit() {
    this.todayDate = this.dateService.gettodaysDate();
  }

}
