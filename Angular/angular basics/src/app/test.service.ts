import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor() { }
  printOnConsole(args) {
    console.log(args);
  }
  gettodaysDate() {
    let ndate = new Date();
    return ndate;
  }
}
