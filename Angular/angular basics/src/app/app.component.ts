 import { Component } from '@angular/core';
import { TestService } from './test.service';
import { HttpClient } from '@angular/common/http';
import { ComponentFactoryResolver } from '@angular/core/src/render3';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-angular-practise';
  dateMessage : string;
  private events = [];
constructor(private svc : TestService,private http:HttpClient) {
  setInterval(() => {
    let currentDate = new Date();
    this.dateMessage = currentDate.toLocaleDateString() + ' ' + currentDate.toLocaleTimeString();
  }
    , 1000)
  svc.printOnConsole("i got the first service");
}
ngOnInit() {
}
months = ["January", "Feburary", "March", "April", "May", 
"June", "July", "August", "September",
"October", "November", "December"];
isavailable = false;
get_events() {
  let obs =  this.http.get('http://192.168.150.66:8080/myspace/displayEvents')
  obs.subscribe((res:any[])=> {
    console.log(res);
    this.events =res;
    console.log(this.events);
  })
}
onclick(event) {
  alert("event is clicked");
  console.log(event)
}
name:string="sowjanya";
onNameKeyUp(event) {
  console.log(event.target.value)
}
changeMonth(event) {
  alert("change month from the dropdown")
  console.log(event)
}
todayDate = new Date();
jsonval = {name:'Rox', age:'25', address:{a1:'Mumbai', a2:'Karnataka'}};
// private todaydate:any ;
// todayDate(){
// this.todaydate = new Date().toLocaleTimeString();
// setInterval(this.todaydate,1000);
// }
cs = 100;

}
