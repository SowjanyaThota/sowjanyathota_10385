import React, { Component } from 'react';
import './postproducts.css';
class AddProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productname: '',
            price: '',
            quantity: '',
            productType: ''
        }
    }
    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
    }
    handleSubmit = event => {
        event.preventDefault();
        console.log("name: " + this.state.productname);
        console.log("price: " + this.state.price);
        console.log("quantity: " + this.state.quantity);
        console.log("producttype: " + this.state.productType);
        const data = {
            productname: this.state.productname,
            price: this.state.price,
            quantity: this.state.quantity,
            productType: this.state.productType
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/addProducts", {
            method: 'POST',
            headers: { 'content-Type': 'application/json' },
            body: JSON.stringify(data)
        })
            .then((res) => res.json)
            .then((data) => console.log("data added succssfully"))
            .catch((err) => console.log(err))
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                <table align="center">
                    <tr>
                    <td>Enter Product Name :</td>
                    <td> <input type="text" name="productname" value={this.state.productname} placeholder="Enter Product Name" onChange={this.handleChange} /></td><br />
                    </tr>
                    <tr>
                    <td> Enter Product Price :</td>
                    <td> <input type="text" name="price" value={this.state.price} placeholder="Enter Product Price" onChange={this.handleChange} /></td><br />
                    </tr>
                    <tr>
                    <td> Enter Product Quantity :</td>
                    <td> <input type="text" name="quantity" value={this.state.quantity} placeholder="Enter Product Quantity" onChange={this.handleChange} /></td><br />
                    </tr>
                    <tr>
                    <td>Product Type (Units or Weight) :</td>
                    <td> <input type="text" name="productType" value={this.state.productType} placeholder="Enter Product Type" onChange={this.handleChange} /></td><br />
                    </tr>
                    <tr >
                    <td><input type="submit" value="submit" /></td>
                    </tr>
                    </table>
                </form>
            </div>
        );
    }
}

export default AddProducts;