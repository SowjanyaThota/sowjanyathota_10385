import React, { Component } from 'react';
import './postproducts.css';
class DeleteProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productname: '',
        }
    }
    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
    }
    handleSubmit = event => {
        event.preventDefault();
        console.log("name: " + this.state.productname);
        const data = {
            productname: this.state.productname,
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/delProducts", {
            method: 'DELETE',
            headers: { 'content-Type': 'application/json' },
            body: JSON.stringify(data)
        })
            .then((res) => res.json)
            .then((data) => console.log("data added succssfully"))
            .catch((err) => console.log(err))
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <table align="center">
                        <tr>
                            <td>Enter Product Name :</td>
                            <td> <input type="text" name="productname" value={this.state.productname} placeholder="Enter Product Name" onChange={this.handleChange} /></td><br />
                        </tr>
                        <tr >
                            <td><input type="submit" value="submit" /></td>
                        </tr>
                    </table>
                </form>
            </div>
        );
    }
}

export default DeleteProducts;