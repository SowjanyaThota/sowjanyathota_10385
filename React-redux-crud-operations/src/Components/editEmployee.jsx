import React, { Component } from 'react';
import * as employeeActions from '../Store/Actions/employeeActions'
class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            salary: '',
            age: ''
        }
    }
onChangeName = (e) => {
    
    this.setState({
        name : e.target.value
    })
}
onChangeSalary = (e) => {
 
    this.setState({
        salary: e.target.value
    })
}
onChangeAge = (e) => {
    
    this.setState({
        age : e.target.value
    })
}

    onSubmit = () => {
        console.log(this.props.match.params.id)
        const id = this.props.match.params.id;
        const data = {
            name: this.state.name,
            salary: this.state.salary,
            age: this.state.age
        }
        this.props.employeeActions.editEmployee(id,data);
        window.location.href = '/'
    }
    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <input type="text" name="name" placeholder="name" value={this.state.name} onChange={this.onChangeName}/><br />
                <input type="text" name="salary" placeholder="salary" value={this.state.salary} onChange={this.onChangeSalary}/><br />
                <input type="text" name="age" placeholder="age" value={this.state.age} onChange={this.onChangeAge} /><br />
                <button className="btn-info mt-2" >Sumbit</button>
            </form>
        );
    }
}

export default Edit;