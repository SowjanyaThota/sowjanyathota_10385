import React, { Component } from 'react';
import * as employeeActions from '../Store/Actions/employeeActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import  {Link} from 'react-router-dom'

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: ''
        }
    }
    componentDidMount() {
        console.log("details componnet")
        debugger;

        this.props.employeeActions.fetchEmployee();
    }
    handleDelete = (id) => {
        if (window.confirm("do u want to delkete an employee")) {
            this.props.employeeActions.deleteEmployee(id);
            setTimeout(() => {
                window.location.reload()
            }, 300)
        }
    }

    render() {
        return (
            <div>
                {
                    (this.props.details)
                        ?
                        <div>
                            <div>
                                {this.props.details.map(detail => (
                                    <div key={detail.id}>
                                        <card className="card card-primary m-2">
                                            <p>{detail.id}</p>
                                            <p>{detail.employee_name}</p>
                                            <p>{detail.employee_salary}</p>
                                            <button className="btn-info mt-2" onClick={this.handleDelete}>Delete</button>

                                            <button className="btn-warning mt-4"><Link to="edit">Edit</Link></button>
                                        </card>
                                    </div>
                                ))}
                            </div>
                        </div> :
                        <div>
                            Loading
                            </div>
                }
            </div>
        );
    }
}
function mapStateToProps(state) {
    debugger;
    console.log(state.employeeReducer.details)
    return {
        details: state.employeeReducer.details
    };
}

function mapDispatchToprops(dispatch) {
    debugger;
    return {
        employeeActions: bindActionCreators(employeeActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(Details);
