import {Route,BrowserRouter} from 'react-router-dom';
import React, { Component } from 'react';
import EmployeeDetails from '../src/Components/employeeDetails'
import EditEmployee from '../src/Components/editEmployee'

class Routers extends Component {
    render() { 
        return ( 
            <BrowserRouter>
            <div>
            <Route path="/" exact component={EmployeeDetails}/>
            <Route path="/edit" component={EditEmployee}></Route>
            </div>
            </BrowserRouter>
         );
    }
}
 
export default Routers;