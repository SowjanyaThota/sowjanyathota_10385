import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../Reducers/rootReducer'
import employeeService from '../middleware/employeeService';

export default function configureStore() {
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            employeeService
        ))
    );
};