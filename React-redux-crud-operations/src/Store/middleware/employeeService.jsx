import * as allActions from '../Actions/actionConstants';
import request from 'superagent';
import * as employeeActions from '../Actions/employeeActions'

const employeeService = (store) => next => action => {
    debugger;
    next(action)
    switch (action.type) {
        case allActions.FETCH_EMPLOYEE:
            request.get("http://dummy.restapiexample.com/api/v1/employees")
                .then(res => {

                    console.log("success");
                    const data = JSON.parse(res.text);
                    console.log(data);
                    next(employeeActions.receiveEmployee(data));
                })
        case allActions.UPDATE_EMPLOYEE_BY_ID:
            request.put("http://dummy.restapiexample.com/api/v1/update/" + `${action.payload.id}`)
                .then(res => {

                    console.log("success");
                    const data = JSON.parse(res.text);
                    console.log(data);
                    next(employeeActions.editSuccess(data));
                })
        case allActions.DELETE_EMPLOYEE:
            request.delete("http://dummy.restapiexample.com/api/v1/employee/" + `${action.payload}`)

                .catch(err => {

                    console.log("service failure");
                    next({ type: 'EVENT_FETCH_ERROR', err });
                })
            break;
        default:
            break
    }
}
export default employeeService;