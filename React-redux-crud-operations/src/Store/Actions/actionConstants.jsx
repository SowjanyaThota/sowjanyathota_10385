export const FETCH_EMPLOYEE = 'FETCH_EMPLOYEE'
export const RECEIVE_EMPLOYEE = 'RECEIVE_EMPLOYEE'
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE'

export const FETCH_EMPLOYEE_BY_ID = 'FETCH_EMPLOYEE_BY_ID'

export const UPDATE_EMPLOYEE_BY_ID = 'UPDATE_EMPLOYEE_BY_ID'
export const UPDATED_SUCCESS = 'UPDATED_SUCCESS'