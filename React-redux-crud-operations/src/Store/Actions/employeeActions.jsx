import * as allActions from '../Actions/actionConstants'

export function fetchEmployee() {
    debugger;
    return {
        type: allActions.FETCH_EMPLOYEE,
        payload: {}
    }
}

export function receiveEmployee(data) {
    debugger;
    console.log(data)
    return {
        type: allActions.RECEIVE_EMPLOYEE,
        payload: data
    }
}
export function deleteEmployee(id) {
    console.log(id)
    return {
        type: allActions.DELETE_EMPLOYEE,
        payload: id
    }
}
export function editEmployee(id,data) {
    debugger;
    return {
        type: allActions.UPDATE_EMPLOYEE_BY_ID,
        payload: {id,data}
    }
}
export function editSuccess(data) {
    debugger;
    return {
        type: allActions.UPDATED_SUCCESS,
        payload: {data}
    }
}