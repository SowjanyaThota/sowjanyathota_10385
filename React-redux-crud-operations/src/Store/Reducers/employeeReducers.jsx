import * as allActions from '../Actions/actionConstants'
const initialise = {
    details: [],
    isLoaded: false,
    updated: false
}
export default function employeeReducer(state = initialise, action) {
    switch (action.type) {
        case allActions.FETCH_EMPLOYEE:
            debugger;
            return action;
        case allActions.RECEIVE_EMPLOYEE:
            console.log("reducer")
            console.log(action.payload)
            debugger;

            return {
                ...state,
                isLoaded: true,
                details: action.payload
            }
        case allActions.DELETE_EMPLOYEE:
            return action;
        case allActions.UPDATED_SUCCESS:
            return {
                ...state,
                updated: true
            }
        default:
            return state;
    }

}