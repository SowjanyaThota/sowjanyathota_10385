import {combineReducers} from 'redux'
import employeeReducer from '../Reducers/employeeReducers'

const rootReducer = combineReducers({
    employeeReducer
})
export default rootReducer
