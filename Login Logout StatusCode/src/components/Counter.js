import React, { Component } from 'react';
import { connect } from 'react-redux';



class Counter extends Component {
    increment = () => {
        this.props.dispatch({ type: 'INCREMENT' });
    }

    decrement = () => {
        this.props.dispatch({ type: 'DECREMENT' });
    }


    render() {
        return (
            <div>
                <h2>Counter</h2>
                <label>
                    <button onClick={this.decrement}>-</button>
                    <span>{this.props.count}</span>
                    <button onClick={this.increment}>+</button>
                </label>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.counterReducer.count
    }
}
export default connect(mapStateToProps)(Counter);
