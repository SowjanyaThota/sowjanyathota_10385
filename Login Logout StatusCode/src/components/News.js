import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as  newsActions from '../store/actions/newsActions'
import { bindActionCreators } from 'redux';
import './information.css'
import NewsHeader from '../components/NewsHeader';
import { Link } from 'react-router-dom';


class News extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.newsActions.fetchNews();
    }

    render() {
        return (
            <div>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10 col-xs-12 main">
                            <p style={{ color: "white" }}>Information about cancer</p>
                            <p style={{ color: "white", fontSize: 15 }}>Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div class="body">
                        <div class="row"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div class="col-sm-3 col-xs-12">
                                <nav class="navbar">
                                    <ul class="nav navbar-brand border" role="tablist">
                                        <li class="nav-item ">
                                            <a class="nav-link active" href="#" >News</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" >NGO'S/NPO'S</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" >Support group</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" >online help</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="news">
                                <div>
                                    <h4 style={{ float: "left" ,marginLeft:"10px",marginTop:"-20px"}}>News</h4>
                                    <h4 style={{ float: "right", marginTop: -20 }}><a href="#" >Show All</a></h4>
                                </div>
                                <hr style={{ marginTop: "px", width: 470, marginRight: "10px", borderTop: "1px solid black", marginLeft: "10px" }}></hr>
                                <div class="col-sm-12 col-xs-12">
                                    {
                                        (this.props.newsItems)
                                            ? <div class="container-fluid ">
                                                {this.props.newsItems.map(item => (

                                                    <div> <div key={item._id} style={{ textAlign: "center" }} class="article">
                                                        <img class="newsLogo" src={"http://13.229.176.226:8001/" + item.image} alt="" />
                                                        <p style={{ fontSize: 18 }}><Link to={'/news/' + item._id}>{item.heading}</Link></p>
                                                        <i class="fa fa-thumbs-up" style={{ float: "right", marginTop: -20, marginRight: 10, color: "blue" }}>0</i>
                                                        <i class="fa fa-comments" style={{ color: "red", marginLeft: 320, marginTop: -20, float: "left" }}>0</i>
                                                        <p style={{ fontSize: 12, color: "black", overflow: "" }}>{item.brief}</p>
                                                    </div><br />
                                                    </div>
                                                ))}
                                            </div>
                                            : <div>
                                                Loading
                                </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news
    };
}

function mapDispatchToprops(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(News);
