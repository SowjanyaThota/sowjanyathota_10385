import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { Redirect } from 'react-router-dom'
import * as userActions from '../store/actions/userAction'

class RedirectPage extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    componentDidMount() {
        this.props.userActions.fetchUser()
    }

    render() {
        switch (this.props.user) {
            case 'user':
                return <Redirect to='/user'></Redirect>
            case 'recruiter':
                return <Redirect to='/recruiter'></Redirect>
            case 'admin':
                return <Redirect to='/admin'></Redirect>
            default:
                return null;
        }
    }
}

function mapStateToProps(state) {
    console.log (state.userReducer.role)
    return {
        user : state.userReducer.role
    };
}

function mapDispatchToprops(dispatch) {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(RedirectPage);