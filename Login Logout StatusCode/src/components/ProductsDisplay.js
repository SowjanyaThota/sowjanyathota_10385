import React, { Component } from 'react'

class ProductDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoaded: false
        }
    }

    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
            .then(response => response.json())
            .then(productdata => {
                this.setState = ({
                isloaded: true,
                products: productdata
            })
            })
    }

    render() {
        var { isloaded, products } = this.state;
        if(!isloaded){
            // <div>Loading</div>
        }
        return (
            <div>
                
            {products.map(product => (
                        <p key="{product.id}">
                           {product.productname} 
                        </p>
                    ))}
           </div>
        )
    }
}
export default ProductDisplay