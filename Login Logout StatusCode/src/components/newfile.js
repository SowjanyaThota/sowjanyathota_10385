import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as  newsActions from '../store/actions/newsActions'
import { bindActionCreators } from 'redux';
import './information.css'

class News extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.newsActions.fetchNews();
    }

    render() {
        return (
            <div>
                <div>
                    <div class="container-fluid head1">
                        <nav class="navbar navbar-expand-md fixed-top head">
                            <button class="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <a class="navbar-brand" href="#">AID FOR CANCER</a>
                            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                <ul class="nav navbar-nav navbar-right ml-auto nav-pills">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">information</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">opinions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">forum</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">volunteer</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">seekhelp</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><button class="btn btn-info btn-md navbar-btn">Sign In</button></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-10 col-xs-12 main">
                                <p style={{ color: "white" }}>Information about cancer</p>
                                <p style={{ color: "white", fontSize: 15 }}>Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                        <div class="body">
                            <div class="row"></div>
                            <div class="col-sm-10 col-xs-12">
                                <div class="col-sm-3 col-xs-12">
                                    <nav class="navbar">
                                        <ul class="nav navbar-brand border" role="tablist">
                                            <li class="nav-item ">
                                                <a class="nav-link active" href="#" >News</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#" >NGO'S/NPO'S</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#" >Support group</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#" >online help</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="news">
                                <h3 style={{marginTop:0}}>News</h3> 
                                <h4 style={{float:"right",marginTop:-30}}><a href="#">Show All</a></h4>
                                <hr style={{size:3}}/>
                                <div class="col-sm-12 ">
                                    {
                                        {/* {this.props.newsItems.map(item => (
                                <div key={item._id} style={{ textAlign: "justify" }}>
                                    <h2>{item._id}</h2>
                                    <h2 align="center" style={{ color: "blue" }}>{item.heading}</h2>
                                    brief:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.brief}</p>
                                    body:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.body}</p>
                                    timestamp:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.timestamp}</p>
                                    tags:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.tags}</p>
                                    comment:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }} >{item.comment}</p><br />
                                </div>
                            ))} */}
                                        (this.props.newsItems)
                                            ? <div class="container-fluid ">
                                                {this.props.newsItems.map(item => (

                                                   <div> <div key={item._id} style={{ textAlign: "center" }} class="article">
                                                        <h6><a href={`/news/${item._id}`}>{item.heading}</a></h6>
                                                    </div><br />
                                                    </div>
                                                ))}
                                            </div>
                                            : <div>
                                                Loading
                                </div>
                                    }
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news
    };
}

function mapDispatchToprops(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(News);
