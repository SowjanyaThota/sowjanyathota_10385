
import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div >
                <nav class="navbar navbar-inverse " style={{ backgroundColor: "#ee6e73",height:"70px"}}>
                    <ul class="nav  navbar-right">
                        <li class="foot-item" style={{marginLeft:"200px"}}>
                            <a class="foot-link" href="#" >Contact Us</a>
                        </li>
                        <li class="foot-item">
                            <a class="foot-link" href="#" style={{marginLeft:"10px",border:"1px solid white",color:"dodgerblue",padding:"5px"}}>Report Abuse</a>
                        </li>
                        <li class="foot-item" style={{marginLeft:"300px"}}>
                            <a class="foot-link" href="#">AboutUs</a>
                        </li>
                        <li class="foot-item" style={{marginLeft:"30px"}}>
                            <a class="foot-link" href="#">Privacy Policy</a>
                        </li>
                        <li class="foot-item" style={{marginLeft:"30px"}}>
                            <a class="foot-link" href="#">Disclaimer</a>
                        </li>
                    </ul>
                </nav>
                <ul style={{backgroundColor:"#e65055",height:"50px"}}>
                    <li style={{marginLeft:"600px",padding:"10px",color:"white",listStyleType:"none"}}>© 2019 AidForCancer</li>
                </ul>
            </div>
                )
            }
        }
export default Footer;
