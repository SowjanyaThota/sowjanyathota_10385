import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../store/actions/userAction';
import { Redirect } from 'react-router-dom';

class adminDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valid : false
        }
    }

    Logout = e =>{
        localStorage.removeItem('jwt-token')
        this.setState ({ valid : true })
    }
    render(){
        if (this.state.valid==true){
            return (
            <Redirect to = "/" />
            )} 
       return (
           
            <div>
                
                 <p>hello admin</p>
                 <div>
                     <button onClick={this.Logout}>Log Out</button>
                 </div>
                 
            </div>
        );
    }
}



export default adminDashboard;
