import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as  authActions from '../store/actions/authActions'
import './Login.css'
import { Redirect } from 'react-router-dom'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isSubmitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();

    }

    doLogin = e => {

        this.props.authActions.doLoginUser({
            email: this.state.email,
            password: this.state.password
        })
        this.setState({
            isSubmitted: true
        })
    }

    render() {
        if (this.state.isSubmitted) {
            console.log(this.props.isLoggedIn)
            if (localStorage.getItem('jwt-token') && (this.props.isLoggedIn === true)) {
            //    window.location.reload;
                return <Redirect to='/redirect' />
            }
            else {

                if (this.props.statuscode == -1) {
                    alert("Email is not registered with us")
                    this.setState({ isSubmitted: false })
                }
                else if (this.props.statuscode == -2) {
                    alert("password format change")
                    this.setState({ isSubmitted: false })
                }
                else if (this.props.statuscode == -3) {
                    alert("password is not correct")
                    this.setState({ isSubmitted: false })
                }
                else if (this.props.statuscode == 422) {
                    alert("invalid emailformat enterthe correct format")
                    this.setState({ isSubmitted: false })
                }

            }
        }
        return (
            <div >
                <div class="body" style={{ width: 280, border: 2, marginLeft: 400, backgroundColor: "#c0c0c0", height: 240 }}>
                    <h3 align="center" style={{ marginTop: 70, marginleft: 50, color: "#e7d7bf" }}>Login Form</h3>
                    <form  >
                        <label>
                            Email   :<br /> <input type="email" name="email" value={this.state.email} onChange={this.handleChange} style={{ borderRadius: 5, width: 200 }} />
                        </label><br />
                        <label>
                            Password:<br /><input type="password" name="password" value={this.state.password} onChange={this.handleChange} style={{ borderRadius: 5, width: 200 }} />
                        </label><br />
                    </form>
                    <input type="submit" value="submit" onClick={this.doLogin} style={{ backgroundColor: "#3bbbb3", color: "white", marginLeft: 90, marginTop: 10, borderRadius: 5 }} />

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.authReducer.is_logged_in,
        statuscode: state.authReducer.statuscode
    };
}

function mapDispatchToprops(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(Login);