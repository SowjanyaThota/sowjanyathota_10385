import request from 'superagent';
import * as allActions from '../actions/actionConstants';
import * as userActions from '../actions/userAction';


const userService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_USER:
            request.get("http://13.250.235.137:8050/api/user")
                .set('Accept', 'application/json')
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    next(userActions.receiveUser(data));
                }
                )
                
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_USER_ERROR', err });
                })

            break;


        default:
            break;
    }
}
export default userService;