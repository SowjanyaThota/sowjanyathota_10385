import * as allActions from '../actions/actionConstants';

const initialise = {
    role:'',
}

export default function userReducer(state = initialise, action) {
    switch (action.type) {
        case allActions.FETCH_USER:
            return action;

        case allActions.RECEIVE_USER:
            return {
                ...state,
                role: action.payload.user.role,
            }
        default:
            return state
    }
}
