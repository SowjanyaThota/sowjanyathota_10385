﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Children2:ChildrenProgram
    {
        public Children2()
        {
            Console.WriteLine("children 2 is called");
        }
       public static void Main()
        {
            Children2 c2 = new Children2();
            c2.Show();
            c2.Area(10,25);
            c2.TestChild();
            Console.ReadLine();
        }
    }
}
