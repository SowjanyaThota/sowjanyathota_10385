﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Polymorphism
{
    class OveridingExample
    {
        public void PrimeNumber()
        {
            int n, count = 0,i;
            Console.WriteLine("enter a number");
             n = Convert.ToInt32(Console.ReadLine());
            for(i=1;i<=n;i++)
            {
                if(i%2 == 0)
                {
                    count++;
                }
                
            }
            if(count == 2)
            {
                Console.WriteLine("prime number");
            }
            else
            {
                Console.WriteLine("not prime");
            }
        }
        public void Test()
        {
            Console.WriteLine("parent method is called");
        }
        public virtual void Show()
        {
            Console.WriteLine("parent method");
        }
        static void Main()
        {
            OveridingExample e = new OveridingExample();
            e.PrimeNumber();
            e.Show();
            Console.ReadLine();
        }
    }
}
