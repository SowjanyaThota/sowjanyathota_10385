﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Polymorphism
{
    class InheritanceBasedOverloading:MethodOverloading
    {
        public void Test(int i,int b,int c)
        {
            Console.WriteLine(i+b+c);
        }
        static void Main()
        {
            InheritanceBasedOverloading i = new InheritanceBasedOverloading();
            i.Test(10, 29, 27);
            Console.ReadLine();
        }
    }
}
