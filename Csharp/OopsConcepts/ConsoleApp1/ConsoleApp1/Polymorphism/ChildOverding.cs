﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Polymorphism
{
    class ChildOverding:OveridingExample
    {
        public override void Show()
        {
            Console.WriteLine("child");
        }
        public void Test(int i)
        {
            Console.WriteLine("child" + i);
        }
        /*
         new keyword in a method is used for hiding or shadowing
             */
        public new void Test()
        {
            Console.WriteLine("child");
        }
        static void Main()
        {
            ChildOverding c = new ChildOverding();
            c.Test();
            c.Test(10);
            c.Show();
            Console.ReadLine();
        }
    }
}
