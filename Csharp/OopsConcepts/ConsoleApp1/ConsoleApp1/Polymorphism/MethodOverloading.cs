﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Polymorphism
{
    public class MethodOverloading
    {
        public void Test()
        {
            Console.WriteLine("method with no parameters");
        }
        public void Test(int i)
        {
            Console.WriteLine("method with one integer parameters"+i);
        }
        public void Test(int i , string s)
        {
            Console.WriteLine("method with string and int  parameters"+s +" " +i);
        }
        public void Test(string s)
        {
            Console.WriteLine("method with string parameters"+s);
        }
        public void Test(string s,int i)
        {
            Console.WriteLine("method with string and int parameters" + s + " " + i);
        }
        static void Main()
        {
            string str = "Hello World";
            MethodOverloading mo = new MethodOverloading();
            Console.WriteLine(str.IndexOf('o'));
            Console.WriteLine(str.IndexOf('o',5));
            
            mo.Test();
            mo.Test(10);
            mo.Test("sowji");
            mo.Test(10, "sowji");
            mo.Test(20, "sowsdh");
            Console.ReadLine();
        }
    }
}
