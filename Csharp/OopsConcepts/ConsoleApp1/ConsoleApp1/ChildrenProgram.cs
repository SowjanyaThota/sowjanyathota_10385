﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ChildrenProgram:Program
    {
        public ChildrenProgram()
        {
            Console.WriteLine("Child constructor is called");
         }

        public void TestChild()
        {
            int a = 0, b = 1, c;
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(a);
            Console.WriteLine(b);
            for (int i = 2; i < n; i++)
            {
                c = a + b;
                Console.WriteLine(c);
                a = b;
                b = c;
            }
        }
        static void Main()
        {

            // Program p = new Program();
            //ChildrenProgram c = p as ChildrenProgram;
            //ChildrenProgram c = new ChildrenProgram();
            // Program p = c;
            /*
             child instance => to => parent => parent reference
             parent instance  => to => child => child not valid
              child instance => to => parent => parent reference => child => child reference => valid
             */
            ChildrenProgram c = new ChildrenProgram();
            Program p = c;
            ChildrenProgram c1 = p as ChildrenProgram; ;
            c1.Show();
            c1.Area(10, 20);
            c1.TestChild();
            Console.WriteLine(c1.GetType());
            object obj = new object();
            Console.WriteLine(obj.GetType());
           
            Console.ReadLine();
        }
    }
}
