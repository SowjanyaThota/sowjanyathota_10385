﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Program
    {
      public Program()
        {
            Console.WriteLine("parent constructor");
        }
        public void Show()
        {
            Console.WriteLine("parent method is called");
        }
        public void Area(int a , int b)
        {
           
            int c = (a * b) / 2;
            Console.WriteLine("area of a right triangle" + c);
        }
    }
}
