import React, { Component } from 'react';
import './FetchProducts.css'
class DisplayAllProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
            .then(response => response.json())
            .then(productsData => {
                this.setState({
                    isLoaded: true,
                    products: productsData
                })
            });
    }
    render() {
        var { isLoaded, products } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div>                
               <table >
                   <caption><h2 style={{textalign:"center"}}>Product Details</h2></caption>
               <tr>
                   <th>Product Id</th>
                   <th>Product Name</th>
                   <th>Price</th>
                   <th>Quantity</th>
                   <th>productType</th>
               </tr>
                    {products.map(product => (
                        <tr key="{product.id}">
                            <td> {product.product_id} </td> 
                            <td> {product.productname} </td>
                           <td> {product.price} </td>
                           <td> {product.quantity} </td>
                           <td>  {product.productType}</td>
                        </tr>
                    ))}
                </table>
            </div>
        );
    }
}
export default DisplayAllProducts;