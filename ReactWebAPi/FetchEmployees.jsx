import React, { Component } from 'react';
import './FetchEmployees.css'
class DisplayAllEmployees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayEmployee')
            .then(response => response.json())
            .then(employeeData => {
                this.setState({
                    isLoaded: true,
                    employees: employeeData
                })
            });
    }
    render() {
        var { isLoaded, employees } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div>
                
                <table align="center">
                <tr>
                            <th>id</th>
                            <th>Ename</th>
                            <th>role</th>
                            <th>favouritefood</th>
                        </tr>
                    {employees.map(employee => (
                        <tr key="{employee.id}">
                        <td> {employee.employeeId}</td>
                           <td > {employee.employeeName} </td>
                           {/* <td> Email: {employee.employeeEmail} </td> */}
                           <td>{employee.role} </td>
                            <td> {employee.securityQuestion} </td>

                        </tr>
                    ))}
                </table>
            </div>
        );
    }
}
export default DisplayAllEmployees;