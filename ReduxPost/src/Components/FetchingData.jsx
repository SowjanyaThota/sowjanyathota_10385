import React, { Component } from 'react';

class FetchingData extends Component {

    constructor(props) {
        super(props);
        this.state = {
            array: {},
            isLoaded: false
        }
    }

    componentWillMount() {
        fetch("https://jsonplaceholder.typicode.com/todos/1")

            .then(response => response.json())
            .then(objdata => {
                this.setState({
                    isLoaded: true,
                    array: objdata
                })
            })
            .catch(err => alert("not success"))
    }

    render() {
        // const dataaa = this.state.array.map(dataa => (
        //     <div key = {dataa.title}>
        //         <p>{dataa.title}</p>
        //     </div>
        // ))
        return (
        
            <div>
                <h1>{this.state.array.title}</h1>
                {/* <h1>{dataaa}</h1> */}
            </div>
                // this.state.data.map(dataa => (
                //     <div>
                //     <p key="{dataa.id}"></p>
                //     <h1>{dataa.title}</h1>
                //    </div>
                // ))
        
        )
    }
}

export default FetchingData;