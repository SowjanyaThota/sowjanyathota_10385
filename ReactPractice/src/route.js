import React from 'react';
import {Switch,Route,BrowserRouter} from 'react-router-dom';


import Clock from './components/clock';
import Register from './components/register';
import Home from './components/home';
import Essay from './components/essayForm';
import Hello from './components/hello';
import Car from './components/car';
import CarDetail from './components/cardetails';


const Routes = () => (
    <BrowserRouter>
        {/* <Switch> */}
        <div>
            <Route path={"/"} exact component={Home}></Route>
            <Route path={"/clock"} component={Clock}></Route>
            <Route path={"/register"} component={Register}></Route>
            <Route path={"/essay"} exact component={Essay}></Route>
            <Route path={"/hello"} exact component={Hello}></Route>
             <Route path={"/cars"} component={Car} exact/>
            <Route path={"/cars/:id"} component={CarDetail} exact/>
            </div>
        {/* </Switch> */}
    </BrowserRouter>
);
 export default Routes;