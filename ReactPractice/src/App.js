import React, { Component } from 'react';
import './App.css';
import Hello from './hello.js';
import Home from './Home.js'
import Clock from './components/clock'
import Register from './components/register';
import Routes from './route';

class App extends Component {
  render() {
    return (
      //  <Hello/>,
      // <Home name='sowjanya'/>
      //  <Clock />
      // <Register />
      <Routes />
    );
  }
}

export default App;
