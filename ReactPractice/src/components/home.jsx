import React, { Component } from 'react';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
        <div style={{textAlign:"center"}}>
            <h1><a href='/clock'>Clock</a></h1><br />
           <h1><a href='/register'>Register</a></h1><br />
           <h1><a href='/essay'>TextAreaForm</a></h1><br />
           <h1><a href='/hello'>hello message</a></h1><br />
           <h1><a href='/cars'>Car</a></h1><br />
        </div>
    )
    }
}
export default Home;