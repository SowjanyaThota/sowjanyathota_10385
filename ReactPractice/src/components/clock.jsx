import React, { Component } from 'react'

class clock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }
    componentDidMount() {
        console.log("did mount called");
        this.timerID = setInterval(
            () => this.tick(),
                5000
        );
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
        console.log("unmount is called");
      }
    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        console.log("render is called");
        return (
            <div>
                <h2>clock</h2>
                <h3> {this.state.date.toLocaleTimeString()}</h3>
            </div>
        )
    }
}
export default clock;