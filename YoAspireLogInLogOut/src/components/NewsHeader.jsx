import React, { Component } from 'react';

class NewsHeader extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container-fluid head1">
                <nav class="navbar navbar-expand-md fixed-top head">
                    <button class="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="#">AID FOR CANCER</a>
                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                        <ul class="nav navbar-nav navbar-right ml-auto nav-pills">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">information</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">opinions</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">forum</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">volunteer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">seekhelp</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><button class="btn btn-info btn-md navbar-btn">Sign In</button></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}
export default NewsHeader;