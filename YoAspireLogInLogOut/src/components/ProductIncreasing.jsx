import React, { Component } from 'react';
import './postproducts.css';
class increaseProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productname: '',
            quantity: ''
        }
    }
    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
    }
    handleSubmit = event => {
        event.preventDefault();
        console.log("name: " + this.state.productname);
        console.log("quantity" + this.state.quantity);
        const data = {
            productname: this.state.productname,
            quantity: this.state.quantity
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/increaseQuantity", {
            method: 'PUT',
            headers: { 'content-Type': 'application/json' },
            body: JSON.stringify(data)
        })
            .then((res) => res.json)
            .then((data) => alert("succesfully increased"))
            .catch((err) => console.log(err))
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                <table align="center">
                    <tr>
                    <td>Enter Product Name :</td>
                    <td> <input type="text" name="productname" value={this.state.productname} placeholder="Enter Product Name" onChange={ (e) => this.handleChange(e)} /></td><br />
                    </tr>
                    <tr>
                    <td> Enter Product Quantity :</td>
                    <td> <input type="text" name="quantity" value={this.state.quantity} placeholder="Enter Product Quantity" onChange={this.handleChange.bind(this)} /></td><br />
                    </tr>
                    <tr >
                    <td><input type="submit" value="submit" /></td>
                    </tr>
                    </table>
                </form>
            </div>
        );
    }
}

export default increaseProducts;