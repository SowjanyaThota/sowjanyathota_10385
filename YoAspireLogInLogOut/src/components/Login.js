import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as  authActions from '../store/actions/authActions'
import './Login.css'
import {Redirect} from 'react-router-dom'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isSubmitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();

    }

    doLogin = e => {
        
        this.props.authActions.doLoginUser({
            email: this.state.email,
            password: this.state.password
        })
        this.setState({
            isSubmitted: true
        })
    }

    render() {
        console.log(this.props.isLoggedIn)
        console.log (this.state.isSubmitted)
        if (this.state.isSubmitted) {
            console.log(this.props.isLoggedIn)
            if (localStorage.getItem('jwt-token') && (this.props.isLoggedIn === true) ) {
                return <Redirect to='/admin/dashboard' />
                
            } 
        }
        return (
            <div class="body" >
                <div style={{ width: 280, border: 2, marginLeft: 400, backgroundColor: "#c0c0c0", height: 240 }}>
                    <h3 align="center" style={{ marginTop: 70, marginleft: 50, color: "#e7d7bf" }}>Login Form</h3>
                    <form  >
                        <label>
                            Email   :<br /> <input type="email" name="email" value={this.state.email} onChange={this.handleChange} style={{ borderRadius: 5, width: 200 }} />
                        </label><br />
                        <label>
                            Password:<br /><input type="password" name="password" value={this.state.password} onChange={this.handleChange} style={{ borderRadius: 5, width: 200 }} />
                        </label><br />
                    </form>
                    <input type="submit" value="submit" onClick={this.doLogin} style={{ backgroundColor: "#3bbbb3", color: "white", marginLeft: 90, marginTop: 10, borderRadius: 5 }} />

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.authReducer.is_logged_in
    };
}

function mapDispatchToprops(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(Login);