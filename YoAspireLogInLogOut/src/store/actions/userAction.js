import * as allActions from '../actions/actionConstants';


export function receiveUser(data) {
    console.log("receive user");
    return {
        type:allActions.RECEIVE_USER, payload : data
    };
}

export function fetchUser() {
    console.log("receive user");
    return {
        type:allActions.FETCH_USER, payload : {}
    };
}