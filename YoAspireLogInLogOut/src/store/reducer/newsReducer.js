import * as allActions from '../actions/actionConstants';

const initialise = {
    news :[],
    newsItem : {},
    isLoaded : false
}

export default function newReducer(state = initialise,action) {
    switch(action.type) {
        case allActions.FETCH_NEWS:
        return action;

        case allActions.RECEIVE_NEWS:
        return  {
         ...state,
         news : action.payload.articles,
         isLoaded :true
        }

        case allActions.FETCH_NEWS_BY_ID:
        return action;

        case allActions.RECEIVE_NEWS_BY_ID:
        return {
            ...state,
            newsItem : action.payload.article,
        }
        default :
        return state
    }
}