import React, { Component } from 'react';
import './footer.css'

class footer extends Component {
    render() {
        return (
            <div>
                <div class="footer-main">
                    <div class="footer">
                        <div class="footer-item">
                            <p>Top Bus Routes</p>
                            <a href="#" target="_blank" title="Hyderabad to Bangalore Bus" style={{ textDecoration: "none" }}>Hyderabad to Bangalore Bus</a><br /><br />
                            <a href="#" target="_blank" title="Bangalore to Chennai Bus" style={{ textDecoration: "none" }}>Bangalore to Chennai Bus</a><br /><br />
                            <a href="#" target="_blank" title="Pune to Bangalore Bus" style={{ textDecoration: "none" }}>Pune to Bangalore Bus</a><br /><br />
                            <a href="#" class="more-link site-links" target="_blank" title="More" style={{ textDecoration: "none" }} >More &gt;</a>
                        </div>
                        <div class="footer-item">
                            <p>Top Cities</p>
                            <a href="#" target="_blank" title="Hyderabad Bus Tickets" style={{ textDecoration: "none" }}>Hyderabad Bus Tickets</a><br /><br />
                            <a href="#" target="_blank" title="Bangalore Bus Tickets" style={{ textDecoration: "none" }}>Bangalore Bus Tickets</a><br /><br />
                            <a href="#" target="_blank" title="Chennai Bus Tickets" style={{ textDecoration: "none" }}>Chennai Bus Tickets</a><br /><br />
                            <a href="#" class="more-link site-links" target="_blank" style={{ textDecoration: "none" }}>More &gt;</a>
                        </div>
                        <div class="footer-item">
                            <p>Top RTC's</p>
                            <a href="#" target="_blank" title="APSRTC" style={{ textDecoration: "none" }}>APSRTC</a><br /><br />
                            <a href="#" target="_blank" title="MSRTC" style={{ textDecoration: "none" }}>MSRTC</a><br /><br />
                            <a href="#" target="_blank" title="HRTC" style={{ textDecoration: "none" }}>HRTC</a><br /><br />
                            <a href="#" class="more-link site-links" target="_blank" style={{ textDecoration: "none" }}>More&gt; </a>
                        </div>
                        <div class="footer-item">
                            <p>Others</p>
                            <a href="#" target="_blank" title="GSRTC" style={{ textDecoration: "none" }}>GSRTC</a><br /><br />
                            <a href="#" target="_blank" title="RSRTC" style={{ textDecoration: "none" }}>RSRTC</a><br /><br />
                            <a href="#" target="_blank" title="KTCL" style={{ textDecoration: "none" }}>KTCL</a><br /><br />
                            <a href="#" class="more-link site-links" target="_blank" title="" style={{ textDecoration: "none" }}>More&gt; </a>
                        </div>
                        <div class="footer-item">
                            <p>Leisure Hotel Cities</p>
                            <a href="#" target="_blank" title="Bangalore SilkBoard Hotels" style={{ textDecoration: "none" }}>Bangalore SilkBoard Hotels</a><br /><br />
                            <a href="#" target="_blank" title="Bangalore Hebbal Hotels" style={{ textDecoration: "none" }}>Bangalore Hebbal Hotels</a><br /><br />
                            <a href="#" target="_blank" title="Chennai Koyambed Hotels" style={{ textDecoration: "none" }}>Chennai Koyambed Hotels </a><br /><br />
                            <a href="#" class="more-link site-links" target="_blank" title="" style={{ textDecoration: "none" }}>More&gt; </a>
                        </div>
                    </div>
                    <div style={{ backgroundColor: "rgb(194, 193, 193)", marginTop: -27, borderTop: 1, width: 988, marginLeft: 10, height: 216 }}>
                        <h3 style={{ marginLeft: 20, marginTop: 20 }} >Top Operators</h3>
                        <ul style={{ float: "left" }}>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>SRS Travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Evacay Bus</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Kallada travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>KPN Travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Orange travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Praveen Travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Rajadhani Travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>VRL Travels</a></li>
                            {/* <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Patel Travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Rajadhani Travels</a></li>
                            <li style={{ listStyleType: "none" }}><a href="#" style={{ color: "black" }}>Morning Travels</a></li> */}
                        </ul>
                        {/* <a href="#" class="more-link site-links" target="_blank" style={{marginTop:-100}}>All Operators &gt;</a> */}
                    </div>
                </div>
                <div class="footer-last" style={{ marginTop: -70 }}>
                    <div class="grid-item2">
                        <h3 style={{ color: "gray" }}>About Red</h3>
                        <a href="#" target="_blank" style={{ color: "white" }}>About Us</a><br /><br />
                        <a href="#" target="_blank" style={{ color: "white" }}>Contact Us</a><br /><br />
                        <a href="#" target="_blank" style={{ color: "white" }}>Mobile Version</a>
                    </div>
                    <div class="grid-item2">
                        <h3 style={{ color: "gray" }}>Global</h3>
                        <a href="#" target="_blank" style={{ color: "white" }}>T &amp; C</a><br /><br />
                        <a href="#" target="_blank" style={{ color: "white" }}>Privacy Policy</a><br /><br />
                        <a href="#" target="_blank" style={{ color: "white" }}>FAQ</a>
                    </div>
                    <div class="grid-item2">
                        <h3 style={{ color: "gray" }}>Infosites</h3>
                        <a href="#" target="_blank" style={{ color: "white" }}>India</a><br /><br />
                        <a href="#" target="_blank" style={{ color: "white" }}>Singapore</a><br /><br />
                        <a href="#" target="_blank" style={{ color: "white" }}>Malaysia</a>
                    </div>
                    <div class="grid-item2" style={{ color: "white" }} >
                        <img src="https://s3.rdbuz.com/web/images/home/sgp/r_logo.png"></img>
                        <p> redBus is the world's largest online bus ticket booking service </p>
                        <p>trusted by over 8 million happy customers globally.</p>
                        <a href="#"><i class="fa fa-facebook-square" style={{ fontSize: 24, color: "white" }}></i></a>
                        <a href="" style={{ marginLeft: 10 }}><i class="fa fa-twitter-square" style={{ fontSize: 24, color: "white" }}></i></a>
                    </div>
                </div>
            </div>
        );
    }
}
export default footer;