import React, { Component } from 'react';
import './busticketbody.css'

let news = [{ img: "https://s3.rdbuz.com/web/images/home/maximum_choices.png", text1: "MAXIMUM CHOICE", text2: "We give you the widest number of travel options across thousands of routes" },
{ img: "https://s1.rdbuz.com/web/images/home/customer_care.png", text1: "SUPERIOR CUSTOMER SERVICE", text2: "We put our experience and relationships to good use and are available to solve your travel issues" },
{ img: "https://s1.rdbuz.com/web/images/home/lowest_Fare.png", text1: "LOWEST PRICES", text2: "We always give you the lowest price with the best partner offers." },
{ img: "https://s2.rdbuz.com/web/images/home/benefits.png", text1: "UNMATCHED BENEFITS", text2: "We take care of your travel beyond ticketing by providing you with innovative and unique benefits." }
]
var fun = news.map(funn => <div class="grid-container5"><div className="grid-item5">
<img src={funn.img}></img>
<p>{funn.text1}</p>
<p>{funn.text2}</p>
</div></div>)

class busticke1 extends Component {
    render() {
        return (
            <div>
                <div class="body1" >
                <h3 style = {{ textAlign: "center", marginTop: 50 }}>why book with RedBus</h3>
                    {fun}
                </div>
                <div class="b2">

                    <div class="table">
                        <h4 class="h2-heading">TOPBUSBOOKINGROUTES</h4>
                        <table>

                            <tr style={{ marginTop: 20 }}>
                                <th>
                                    <span>               Select source: </span>
                                    <select id="sourceDrop" onchange="showRoutes(this)">
                                        <option value="Bangalore">Bangalore</option>
                                        <option value="Chennai">Chennai</option>
                                        <option value="Hyderabad">Hyderabad</option>
                                        <option value="Pune">Pune</option>
                                        <option value="Mumbai">Mumbai</option>
                                        <option value="Delhi">Delhi</option>
                                        <option value="Coimbatore">Coimbatore</option>
                                        <option value="Goa">Goa</option>
                                        <option value="Indore">Indore</option>
                                        <option value="Ahmedabad">Ahmedabad</option>
                                        <option value="Vijayawada">Vijayawada</option>
                                        <option value="Madurai">Madurai</option>
                                        <option value="Nagpur">Nagpur</option>
                                        <option value="Visakhapatnam">Visakhapatnam</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>ROUTE</th>
                                <th colspan="2">FARE (starting)</th><br /><br />
                            </tr>
                            <tr class="Bangalore city-data" >
                                <td>Banglore to Chennai</td>
                                <td>Rs.300</td>
                                <td><a id="time-btn0" href="#" > Book Now </a></td><br /><br />
                            </tr>
                            <tr class="Bangalore city-data" >
                                <td>Bangalore to Hyderabad</td>
                                <td>Rs.490</td>
                                <td><a id="time-btn1" href="#"> Book Now </a></td><br /><br />
                            </tr>
                            <tr class="Bangalore city-data">
                                <td>Bangalore to Pune</td>
                                <td>Rs.714</td>
                                <td><a id="time-btn2" href="#"> Book Now </a></td><br /><br />
                            </tr>
                            <tr class="Bangalore city-data" >
                                <td>Bangalore to Coimbatore</td>
                                <td>Rs.350</td>
                                <td><a id="time-btn3" href="#" > Book Now </a></td><br /><br />
                            </tr>
                            <tr class="Bangalore city-data" >
                                <td>Bangalore to Madurai</td>
                                <td>Rs.460</td>
                                <td><a id="time-btn4" href="#" > Book Now </a></td><br /><br />
                            </tr>
                        </table>
                    </div>
                    <div class="box">
                        <h4 class="h2-heading">MTICKETVSETICKETVSCOUNTERTICKET</h4>
                        <div class="table1">
                            <table>
                                <tr>
                                    <td><img class="box1-img" src="https://s1.rdbuz.com/build/seo/images/M-ticket.png" alt="redBus M Ticket Image" />An mticket is an SMS that is sent to your mobile after booking a bus ticket.</td>
                                </tr>
                                <tr>
                                    <td><img class="box1-img" src="https://s1.rdbuz.com/build/seo/images/E-ticket.png" alt="redBus E Ticket Image" />This is an electronic ticket issued and sent to your email after confirmation</td>
                                </tr>
                                <tr>
                                    <td><img class="box1-img" src="https://s1.rdbuz.com/build/seo/images/C-ticket.png" alt="redBus counter Ticket Image" />Counter Ticket is the hard copy of your bus ticket required while boarding the bus.</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="b3">
                    <div class="comparison-container" style={{ marginTop: 550 }}>
                        <h6 class="h2-heading" style={{ marginTop: 340, marginBottom: 40, marginLeft: 150 }}>ONLINE BUS TICKET BOOKING VS OFFLINE BUS TICKET BOOKING</h6>
                        <div class="com1" style={{ float: "left", margintop: 50 }}>
                            <ul class="online-list">
                                <li style={{ position: "relative", listStyleType: "none" }}>Hassle-free bus booking from anywhere &amp; anytime</li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>Wide choice of bus seats, bus types &amp; operators.</li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>Great offers and cashbacks on booking tickets</li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>Unique benefit for women seat selection</li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>24/7 Friendly Customer Care assistance</li><br /><br />
                            </ul>
                        </div>
                        <div class="divider"></div>
                        <div class="com2" style={{ float: "right", margintop: 50 }}>
                            <ul class="offline-list">
                                <li style={{ position: "relative", listStyleType: "none" }}>Hassle of reaching bus counter &amp; waiting in long queues </li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>Not much choice suiting your preference</li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>No discounts</li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>No unique benefit for women</li><br /><br />
                                <li style={{ position: "relative", listStyleType: "none" }}>No Friendly assistance</li><br /><br />
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default busticke1;