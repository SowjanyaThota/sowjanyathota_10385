import React, { Component } from 'react';
import Body from './body';
import Main from './mainbody';
import {Link} from "react-router-dom";
import './header.css';

class redbus extends Component {
    render() {
        return (
            <div style={{ overflowX: "hidden" }}>
                <div >
                    <ul class="navbar">
                        <li><a href="#"><img class="logo" src="https://image.slidesharecdn.com/redbus-shikhabhansali-170628081104/95/digital-marketing-strategy-by-shikha-bhansali-redbus-1-638.jpg?cb=1499167470" /></a></li>
                        <li><Link to="/bustickets">BUS TICKETS</Link></li>
                        <li><a href="#">HOTELS</a></li>
                        <li><a href="#">BUS HIRE</a></li>
                        <li><a href="#">PILGRIMAGES</a></li>
                        <li><a href="#">CARS<sup>outstation</sup></a></li>
                        <li>  <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" style={{color:"white"}}>ManageBooking
                       <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li class="dropitem"> <a href="#">Bus Schedule</a></li>
                                <li class="dropitem"> <a href="#">Cancelation</a></li>
                            </ul>
                        </div>
                        </li>
                        <li class="lihelp"><a href="#" >Help</a></li>
                    </ul>
                </div >
                <div >
                    <img src="https://st.redbus.in/Images/TRAVEL/home.png"></img>
                    <div class="element">
                        <table style={{ marginLeft: 200 }}>
                            <tr>
                                <td><input type="text" placeholder="FROM" style={{ height: 35, width: 140 }}></input></td>
                                <td><input type="text" placeholder="TO" style={{ marginLeft: -5, height: 35, width: 140 }}></input></td>
                                <td><input type="date" placeholder="ONWARD DATE" style={{ marginLeft: -5, height: 35, width: 140 }}></input></td>
                                <td><input type="date" placeholder="RETURN DATE" style={{ marginLeft: -5, height: 35, width: 140 }}></input></td>
                                <td><button id="btn" style={{ marginLeft: -5, height: 35, width: 140 }}>search buses</button></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="body">
                    <div class="main" style={{backgroundColor:"white",width:500}}>
                        <div class="rebimg">
                            <img src="https://s1.rdbuz.com/Images/TRAVEL/100x100.png" style={{ marginLeft: 0,marginTop: -10 }}></img>
                        </div>
                        <div class="content" >
                            <div style={{ marginLeft: 140 }}>
                                <h6>SAVE BIG ON YOUR BUS TICKETS</h6>
                            </div>
                            <div style={{ marginLeft: 100 }}>
                                <h6 style={{ color: "gray" }}>use code FIRST on app and save upto Rs.350</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <Body />
                <Main />
            </div >
        );
    }
}
export default redbus;
