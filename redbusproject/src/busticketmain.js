import React, { Component } from 'react';
import './busticketmain.css'
class busticket2 extends Component {
    render() {
        return (
            <div class="main1" style={{backgroundColor:"rgb(236, 240, 241)",marginLeft:60,marginBottom:-200}}>
                <div class="mainbody" >
                    <div class="intercontainer">
                        <div class="head">
                            <h2 class="h2-heading" style={{textAlign:"center",marginLeft:-500,marginTop:100}}>FIND BUS TICKET WITH REDBUS</h2>
                            <div class="description" style={{ width: 400, float: "left",marginTop:50,marginLeft:20 }} >
                                redBus offers online bus tickets booking option and also promotes hassle-free bus travel across India. Undergo a quick bus ticketing procedure,
                                 simply choose suitable bus seats, bus type and bus operator then fulfil secured online payment in just few seconds. redBus develops incredible travelling experience from selecting bus type,
                                   </div>
                            <div class="description" style={{ width: 400, float: "left",marginTop:50,marginLeft:50 }}>
                                No more waiting for long hours at bus counters, go ahead; order bus tickets to have a hassle-free booking experience.
                                There are more than 2300+ operators listed in redBus site. They provide service throughout most of the cities in India.
                            </div>
                        </div>
                        <div class="a" style={{ width: 800, height: 300, marginTop: 200,marginLeft:50 }}>
                            <p style={{marginLeft:50}}><b>redBus offers impeccables travels services connecting Indian cities together, top routes are :</b></p>
                            <ul class="bus-ticket">
                                <li><a href="#" style={{color:"black"}}>Bangalore To Hyderabad Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Delhi To Manali Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Mumbai To Goa Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Pune To Goa Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Bangalore To Chennai Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Delhi To Jaipur Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Bangalore To Goa Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Pune To Mumbai Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Bnaglore To Tirupathi Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Delhi To Shimla Bus</a></li>
                                <li><a href="#" style={{color:"black"}}>Chennai To Tirupathi Bus</a></li>
                                <li><a class="more-link" href="#" style={{color:"black"}}>All Routes &gt;</a></li>
                            </ul></div>
                            <div class="a1" style={{ width: 800, height: 200,marginTop: 30,marginLeft:50}}>
                        <p style={{marginLeft:50}}><b>A few of the many bus operators that boosts up the redBus inventory are :</b></p>
                            <ul class="bus-ticket">
                                <li><a href="#" style={{color:"black"}}>SRS Travels</a></li>
                                <li><a href="#" style={{color:"black"}}>VRL Travels</a></li>
                                <li><a href="#" style={{color:"black"}}>KPN Travels</a></li>
                                <li><a href="#" style={{color:"black"}}>Neeta Travels</a></li>
                                <li><a href="#" style={{color:"black"}}>Komitla</a></li>
                                <li><a href="#" style={{color:"black"}}>SRM Travels</a></li>
                                <li><a href="#" style={{color:"black"}}>Parveen Travels</a></li>
                                <li><a href="#" style={{color:"black"}}>Patel Travels</a></li>
                                <li><a href="#" style={{color:"black"}}>Kallada Travels</a></li>
                                <li><a class="more-link" href="#" style={{color:"black"}}>All Operator &gt;</a></li>
                            </ul>
                        </div>
                        <div class="a3" style={{ width: 800, height: 200,marginTop:30,marginLeft:50 }}>
                            <p style={{marginLeft:50}}><b>Some of the well-known cities where redBus is costantly catering its bus services are :</b></p>
                            <ul class="bus-ticket">
                                <li><a href="#" style={{color:"black"}}>Pune Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Delhi Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Kolkata Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Chennai Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Hyderabad Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Bangalore Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Mumbai Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Coorg Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Munnar Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}> Ooty Bus Tickets</a></li>
                                <li><a href="#" style={{color:"black"}}>Bhubaneswar Bus Tickets</a></li>
                                <li><a class="more-link" href="#">All Cities &gt;</a></li>
                            </ul>
                        </div>
                        <div class="a2" style={{ width: 800, height: 300 ,marginTop: 30,marginLeft:50}}>
                            <p style={{marginLeft:50,marginTop:50}}><b>A few of the many RTCs bus operators that boosts up the redBus inventory are :</b></p>
                            <ul class="bus-ticket">
                                <li><a href="#" style={{color:"black"}}></a></li>
                                <li><a href="#" style={{color:"black"}}>APSRTC</a></li>
                                <li><a href="#" style={{color:"black"}}>HRTC</a></li>
                                <li><a href="#" style={{color:"black"}}>UPSRTC</a></li>
                                <li><a href="#" style={{color:"black"}}>RSRTC</a></li>
                                <li><a href="#" style={{color:"black"}}>GSRTC</a></li>
                                <li><a href="#" style={{color:"black"}}>OSRTC</a></li>
                                <li><a href="#" style={{color:"black"}}>PEPSU</a></li>
                                <li><a href="#" style={{color:"black"}}>JKSRTC</a></li>
                                <li><a href="#" style={{color:"black"}}>KTCL</a></li> </ul>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
export default busticket2;