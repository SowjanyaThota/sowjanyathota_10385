import React, { Component } from 'react';
import './body.css'
let news = [{ img: "https://s3.rdbuz.com/web/images/home/maximum_choices.png", text1: "MAXIMUM CHOICE", text2: "We give you the widest number of travel options across thousands of routes" },
{ img: "https://s1.rdbuz.com/web/images/home/customer_care.png", text1: "SUPERIOR CUSTOMER SERVICE", text2: "We put our experience and relationships to good use and are available to solve your travel issues" },
{ img: "https://s1.rdbuz.com/web/images/home/lowest_Fare.png", text1: "LOWEST PRICES", text2: "We always give you the lowest price with the best partner offers." },
{ img: "https://s2.rdbuz.com/web/images/home/benefits.png", text1: "UNMATCHED BENEFITS", text2: "We take care of your travel beyond ticketing by providing you with innovative and unique benefits." }
]
var fun = news.map(funn => <div class="gridcontainer"><div className="griditem">
<img src={funn.img}></img>
<p>{funn.text1}</p>
<p>{funn.text2}</p>
</div></div>)

class body extends Component {
    render() {
        return (
            <div>
                <div class="body1" >
                    <div class="b1 b" style={{marginLeft:10,marginTop:-200}}>
                        <div>
                            <p style={{ textAlign: "center" }}>save upto Rs.225 on Bus Tickets</p>
                        </div>
                        <div>
                            <img src="https://s1.rdbuz.com/Images/MobileOffers/CINDIA/offer-tile.png" style={{ height: 100,marginLeft:60 }} />
                        </div>
                        <div>
                            <p style={{ textAlign: "center" }}>pay using freecharge </p>
                            <p style={{ textAlign: "center" }}>Code : SUPERSAVE</p>
                        </div>
                    </div>
                    <div class="b1 bs" style={{marginLeft:70,marginTop:-200}}>
                        <div>
                            <p style={{ textAlign: "center" }}>save upto Rs.225 on Bus Tickets</p>
                        </div>
                        <div>
                            <img src="https://st.redbus.in/Images/freecharge/Freecharge-offer-tile-247x147.png" style={{ height: 100,marginLeft:60 }} />
                        </div>
                        <div>
                            <p style={{ textAlign: "center" }}>pay using freecharge </p>
                            <p style={{ textAlign: "center" }}>Limited Period Offer</p>
                        </div>
                    </div>
                    <div class="b1 bsss" style={{marginLeft:700,marginTop:-225}}>
                        <p style={{ textAlign: "center" }}>Cashback upto Rs.325 on Bus Tickets </p>
                        <div>
                            <img src="https://s1.rdbuz.com/Images/TRAVEL/274x147.png" style={{ height: 105,marginLeft:60 }} />
                        </div>
                        <div>
                            <p style={{ textAlign: "center" }}>pay using freecharge </p>
                            <p style={{ textAlign: "center" }}>Code : TRAVEL19CB</p>
                        </div>
                    </div>
                </div>

                <div style={{ marginLeft: 200 }}>
                    <img src="https://s3.rdbuz.com/Images/desktop-ad-banner.png" style={{ marginLeft: -200, width: 1000, marginTop: 110 }}></img> </div>
                <div class="video_section" style={{ marginTop: -205, marginLeft: 20, float: "left" }} >
                    <iframe width="375" height="198" src="https://www.youtube.com/embed/x5EQNUsYNUM" alt=""> </iframe>
                </div>

                <div class="mainb" style={{ marginTop: 0 }}>
                    <div class="mainbody">
                        <img src="https://s2.rdbuz.com/web/images/home/city_scape_app_download.png" alt="" style={{ width: 1100, height: 700, opacity: 0.3 }} />
                        <div style={{ marginTop: -625, marginLeft: 30, float: "left", width: 350 }}>
                            <h2>CONVENIENCE ON-THE-GO.</h2>
                            <p >Exclusive features on mobile include</p>
                            <p>redBus NOW - when you need a bus in the next couple of hours. Board a bus on its way.</p>
                            <p>Boarding Point Navigation - Never lose your way while travelling to your boarding point! </p>
                            <p>1-click Booking - Save your favourite payment options securely on redBus, and more.</p>
                            <p>Download the app.</p>
                            <a href="#"><i class="fa fa-apple" style={{ fontSize: 29, marginLeft: 20, color: "black" }}></i></a>
                            <a href="#"></a>
                        </div>
                        <div>
                            <img src="https://s1.rdbuz.com/web/images/home/IOS_Android_device.png" style={{ float: "right", marginTop: -625, width: 600, height: 600 }}></img>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="bodyh" style={{ backgroundColor: "gray",height:600 }}>
                        <img src="https://s1.rdbuz.com/web/images/home/promise.png" style={{ marginLeft: 430 }} />
                        <h2 class="h1class">WE PROMISE TO DELIVER</h2>
                        <div style={{marginLeft:80}}>
                          {fun}   
                          </div>                   
                    </div>
                </div>
            </div>
        );
    }
}
export default body;