import React, { Component } from 'react';
import './mainbody.css';
let Award = [{ img: "https://s2.rdbuz.com/web/images/home/awards/Business_Standard1.png", text: "Most Innovative Company" },
            { img: "https://s1.rdbuz.com/web/images/home/awards/Brand_Trust_Report.png", text: "Most Trusted Brand" },
            { img: "https://s3.rdbuz.com/web/images/home/awards/Eye_for_Travel1.png", text: "Most Innovative Award" },
            ]
var fun1 = Award.map(funn =>
    <div class="grid-container">
        <div className="grid-item">
            <img src={funn.img}></img>
            <p><a class="tex" href="">{funn.text}</a></p>
        </div></div>)


let flag = [{ img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLGRiFYBAx7dcBGISc0iyPzFaMJm29QddGyv5zb02JsLDtXmNNZw", text: "COLOMBIA" },
            { img: "https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/255px-Flag_of_India.svg.png", text: "INDIA" },
            { img: "https://vignette.wikia.nocookie.net/duranduran/images/8/8d/Indonesia-flag_wikipedia_duran_duran.gif/revision/latest?cb=20150401095526", text: "INDONESIA" },
            ]
var funflag = flag.map(funn =>
    <div class="flag-container">
        <div className="flag-item">
            <img src={funn.img} class="img1"></img>
            <p>{funn.text}</p>
        </div></div>)

class main extends Component {
    render() {
        return (
            <div >
                <div style={{ marginLeft: 100, marginTop: 100,marginBottom:270,backgroundColor:"white",height:600 }}>
                    <h2 class="h1class">AWARDS & RECOGNITION</h2>
                    <div style={{marginTop:30}}>
                        {fun1}
                    </div>
                </div>
                <div style={{backgroundColor:"gray",height:450}}>
                <div style={{ marginLeft: 10, marginTop: 100 }}>
                    <h2 class="h1class">OUR GLOBAL PRESENCE</h2>                 
                    {funflag}                    
                </div>
                </div>
                <div>
                    <h2 class="h1class" style={{ color: "lightblack" }}>THE NUMBERS ARE GROWING!</h2>
                    <div class="num-container">
                        <div class="num-item"><p>CUSTOMERS</p>
                            <h1 style={{ color: "red" }}>08 M</h1>
                            <p>redBus is trusted by over 8 million happy customers globally</p></div>
                        <div class="num-item"><p>OPERATORS</p>
                            <h1 style={{ color: "red" }}>2300</h1>
                            <p>network of over 2300 bus operators worldwide</p>
                        </div>
                        <div class="num-item">
                            <p>BUS TICKETS</p>
                            <h1 style={{ color: "red" }}>100+ M</h1>
                            <p>Over 100 million trips booked using redBus</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default main;