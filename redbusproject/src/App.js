import React, { Component } from 'react';
import logo from './logo.svg';
import Headernav from './header';
import Busticketheader from './busticketheader';
import Footer from './footer';
import Main from './mainbody';
import {Route} from 'react-router-dom';
import Busticket2 from './busticketmain';
// import New from './new'

import './App.css';


class App extends Component {
  render() {
    return (
      
      <div>
        <div>
          <Route path="/" exact component={Headernav}></Route>
          <Route path="/bustickets" component={Busticketheader}></Route>
        </div>
        <Footer />
        {/* <New /> */}
      </div>
    );
  }
}

export default App;
