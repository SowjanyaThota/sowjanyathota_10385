import React, { Component } from 'react';
import Busbody  from './busticketbody';
import BusMain from './busticketmain';
import {Link} from "react-router-dom";
import './bustickets.css'

class busticket extends Component {
    render() {
        return (
            <div style={{ overflowX: "hidden",overflowY:"hidden",backgroundColor:"rgb(236,240,241)"}}>
                <div >
                    <ul class="navbar">
                        <li><a href="#"><img class="logo" src="https://image.slidesharecdn.com/redbus-shikhabhansali-170628081104/95/digital-marketing-strategy-by-shikha-bhansali-redbus-1-638.jpg?cb=1499167470" /></a></li>
                        <li><a href="#" class="active">BUS TICKETS</a></li>
                        <li><a href="#">HOTELS</a></li>
                        <li><a href="#">BUS HIRE</a></li>
                        <li><a href="#">PILGRIMAGES</a></li>
                        <li><a href="#">CARS<sup>outstation</sup></a></li>
                        <li class="lihelp"><a href="#" >print/smsTicket</a></li>
                        <li class="lihelp"><a href="#" >Help</a></li>
                    </ul>
                </div >
                <div class="some">
                    <ul>
                        <li style={{ listStyleType: "none" }}><Link to="/"><h6 style={{color:"white"}}>Home&gt;bustickets</h6></Link> </li>
                    </ul>
                </div>
                <div >
                    <img src="https://st.redbus.in/Images/TRAVEL/home.png" style={{ width: 1100, height: 400, marginTop: -20 }}></img>
                    <div class="element">
                        <table style={{ marginLeft: 200 }}>
                            <tr>
                                <td><input type="text" placeholder="FROM" style={{ height: 35, width: 140 }}></input></td>
                                <td><input type="text" placeholder="TO" style={{ marginLeft: -5, height: 35, width: 140 }}></input></td>
                                <td><input type="date" placeholder="ONWARD DATE" style={{ marginLeft: -5, height: 35, width: 140 }}></input></td>
                                <td><input type="date" placeholder="RETURN DATE" style={{ marginLeft: -5, height: 35, width: 140 }}></input></td>
                                <td><button id="btn" style={{ marginLeft: -5, height: 35, width: 140 }}>search buses</button></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="body" >
                    <div class="main" style={{backgroundColor:"white",width:500}}>
                        <div class="rebimg">
                            <img src="https://s1.rdbuz.com/Images/TRAVEL/100x100.png" style={{ marginLeft: 20, marginTop: -10 }}></img>
                        </div>
                        <div class="content" >
                            <div style={{ marginLeft: 140 }}>
                                <h6>SAVE BIG ON YOUR BUS TICKETS</h6>
                            </div>
                            <div style={{ marginLeft: 100 }}>
                                <h6 style={{ color: "gray" }}>use code FIRST on app and save upto Rs.350</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body" >
                    <div class="b1" style={{backgroundColor:"white"}}>
                        <span style={{ marginLeft: 10 }}>Cashback upto Rs.325 on Bus Tickets  </span>
                        <div>
                            <img src="https://s1.rdbuz.com/Images/TRAVEL/274x147.png" style={{ height: 130 }} />
                        </div>
                        <div>
                            <p style={{ textAlign: "center" }}>pay using freecharge  </p>
                            <p style={{ textAlign: "center" }}>Code : TRAVEL19CB</p>
                        </div>
                    </div>
                </div>
                <Busbody />
                <BusMain />
            </div >
        );
    }
}
export default busticket;