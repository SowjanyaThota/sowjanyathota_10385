import React, {Component} from 'react';
const initialState = {
    age:21,
    history : []
}

const Reducer = (state = initialState ,action) => {
    const newState = {...state };

    // if(action.type === 'Age_Up') {
    //     newState.age++;
    // }
    // if(action.type === 'Age_Down') {
    //     newState.age--;
    // }
    // return newState;

    switch(action.type) {
        case "Age_Up" :
        return { ...state,
            age: state.age + action.value,
            history : state.history.concat({id:Math.random(),age:state.age + action.value})
        }
        break;
        case "Age_Down" :
        return { ...state,
            age: state.age - action.value,
            history : state.history.concat({id:Math.random(),age:state.age - action.value})
        }
        break;
        case "DEL" : 
        return {
            ...state,
            history:state.history.filter(el => el.id !== action.key)
        }
        default:return state
    }
}

export default Reducer;