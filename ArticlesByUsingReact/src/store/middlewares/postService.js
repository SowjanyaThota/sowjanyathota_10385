import request from 'superagent';
import * as allActions from '../actions/actionConstants';
import * as postActions from '../actions/postNewsActions';

const newService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.POST_NEWS:
        console.log("news service");
        request.post("http://13.229.176.226:8001/api/news/recent")
        .type('application/json')
        .then(res => {
            console.log("success");
            const data = JSON.parse(res.text);
            console.log(data);
            next(newsActions.postNews(data));
        })
        .catch(err => {
            console.log("service failure");
            next({ type : 'FETCH_NEWS_DATA_ERROR', err });
        })
        break;

        default:
        break;
    }
}
export default newService;