import { combineReducers } from 'redux';

import counterReducer from './counterReducer';

import newsReducer from './newsReducer';

import authReducer from './authReducer';

import userReducer from './userReducer';
import Reducer from '../../ReduxPractise/Store/Reducer';
import ReducerA from '../../ReduxPractise/Store/ReducerA'
import ReducerB from  '../../ReduxPractise/Store/ReducerB';

const rootReducer = combineReducers({
    counterReducer,
    newsReducer,
    authReducer,
    userReducer,
    ReducerA,
    ReducerB
});

export default rootReducer;