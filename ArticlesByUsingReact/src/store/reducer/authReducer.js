import * as allActions from '../actions/actionConstants';

const initialise = {
    is_logged_in : false,
    statuscode : 0
}

export default function authReducer(state = initialise,action) {
    console.log('auth reducer');
    switch(action.type) {
        case allActions.DO_LOGIN_USER:
        console.log('do login user')
        return action;
        case allActions.LOGIN_USER_SUCCESS:
        console.log('do login success')
        return  {
            ...state,
            is_logged_in :true
           }
        case allActions.LOGIN_USER_DATA_ERROR:
           return {
                is_logged_in : false,
                statuscode : action.payload

           }

        default :
        return state;
    }
}