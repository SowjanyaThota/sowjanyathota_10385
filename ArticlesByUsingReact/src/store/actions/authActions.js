import * as allActions from '../actions/actionConstants';

export function doLoginUser(data) {
    console.log('do login user action')
    return {
        type:allActions.DO_LOGIN_USER, payload : data
    };
}

export function loginUserSuccess(data) {
    console.log('login user success action')
    return {
        type:allActions.LOGIN_USER_SUCCESS, payload : data
    };
}

// export function logoutUser() {
//     return {
//         type:allActions.LOGout_USER, payload : {}
//     };
// }