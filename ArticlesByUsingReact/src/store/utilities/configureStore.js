import {createStore,applyMiddleware,compose} from 'redux';
import rootReducer from '../reducer/rootReducer';


import newsMiddleware from '../middlewares/newsService';
import authMiddleware from '../middlewares/authService';
import userMiddleware from '../middlewares/userService';

import MyMiddleware from '../../ReduxPractise/Store/Middleware'

export default function configureStore() {
return createStore(
    rootReducer,
   compose(applyMiddleware(
       newsMiddleware,
       authMiddleware,
       userMiddleware,
       MyMiddleware
   ))
);
}