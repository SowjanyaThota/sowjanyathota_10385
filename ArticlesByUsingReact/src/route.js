import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';


import Clock from './components/clock';
import Register from './components/register';
import Home from './components/home';
import Essay from './components/essayForm';
import Hello from './components/hello';
import Car from './components/car';
import CarDetail from './components/cardetails';
import Employee from './components/FetchEmployees';
import Products from './components/FetchProducts';
import PostProducts from './components/PostProducts';
import UpdateProducts from './components/ProductIncreasing';
import DeleteProducts from './components/DeleteProducts'
import Product from './components/Products'
import Counter from './components/Counter'
import News from './components/News'
import NewsDetails from './components/News_Details'
import Login from './components/Login'
import Admin from './components/adminDashboard'
import redirectPage from './components/redirectPage';
import User from './components/roleDashboard';
import Recruiter from './components/Dashboard';


const Routes = () => (
    <BrowserRouter>
        {/* <Switch> */}
        <div>
            {/* <Route path={"/"} exact component={Login}></Route>
            <Route path={"/redirect"} component={redirectPage} exact></Route>
            <Route path={"/user"} component={User} exact></Route>
            <Route path={"/admin"} component={Admin} exact></Route>
            <Route path={"/recruiter"} component={Recruiter} exact></Route> */}
            {/* <Route path={"/login"} exact components={Login}></Route> */}
            <Route path={"/"} exact component={News}></Route>
            <Route path={"/news/:id"}  exact component={NewsDetails}></Route>

            {/* <Route path={"/clock"} component={Clock}></Route>
            <Route path={"/register"} component={Register}></Route>
            <Route path={"/essay"} exact component={Essay}></Route>
            <Route path={"/hello"} exact component={Hello}></Route>
            <Route path={"/cars"} component={Car} exact />
            <Route path={"/cars/:id"} component={CarDetail} exact /> */}
            {/* <Route path={"/Product"} component={Products}></Route>
            <Route path={"/Employee"} component={Employee}></Route> */}
            {/* <Route path={"/Counter"} component={Counter}>counter</Route> */}


        </div>
        {/* </Switch> */}
    </BrowserRouter>
);
export default Routes;