import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as  newsActions from '../store/actions/newsActions'
import { bindActionCreators } from 'redux';

class News extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.newsActions.fetchNews();
    }

    render() {
        return (
            <div>
                {
                    (this.props.newsItems)
                        ? <div class="container-fluid">
                            {this.props.newsItems.map(item => (
                                <div key={item._id} style={{ textAlign: "justify" }}>
                                    <h2>{item._id}</h2>
                                    <h2 align="center" style={{ color: "blue" }}>{item.heading}</h2>
                                    brief:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.brief}</p>
                                    body:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.body}</p>
                                    timestamp:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.timestamp}</p>
                                    tags:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }}>{item.tags}</p>
                                    comment:<p style={{ fontFamily: "Arial, Helvetica, sans-serif" }} >{item.comment}</p><br />
                                </div>
                            ))}
                        </div>
                        : <div>
                            Loading
                         </div>
                }
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news
    };
}

function mapDispatchToprops(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(News);
