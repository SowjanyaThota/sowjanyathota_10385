import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as  newsActions from '../store/actions/newsActions'
import { bindActionCreators } from 'redux';

import {Link} from 'react-router-dom'




// const id = this.props.match.params.id;
// console.log("id: "+id);



class NewsById extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.newsActions.fetchNewsById();
    }



    render() {
        return (
            <div>
                {
                    (this.props.newsItems)
                        ? <div class="container-fluid">
                            {this.props.newsItems.map(item => (
                                <div key={item._id} style={{ textAlign: "center" }}>
                                    <h2><Link to="">{item._id}</Link></h2>
                                   </div>
                            ))}
                        </div>
                        : <div>
                            Loading
                     </div>
                }
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news
    };
}

function mapDispatchToprops(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(NewsById);
