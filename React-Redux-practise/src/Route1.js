import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';



import Products from './components/FetchProducts';
import PostProducts from './components/PostProducts';
import UpdateProducts from './components/ProductIncreasing';
import DeleteProducts from './components/DeleteProducts';
import Product from './components/Products'


import AgeCounter from './ReduxPractise/AgeCounter'
import Multiple from './ReduxPractise/Multiple'


const Routes = () => (
    <BrowserRouter>
        {/* <Switch> */}
        <div>
            <Route path={"/"} exact component={Multiple}></Route>

        {/* <Route path={"/"} exact component={Product}></Route>
            <Route path={"/Products"} exact component={Products}></Route>
            <Route path={"/PostProducts"} component={PostProducts}></Route>
            <Route path={"/Update"} component={UpdateProducts}></Route>
            <Route path={"/DeleteProducts"} component={DeleteProducts}></Route> */}
        </div>
        {/* </Switch> */}
    </BrowserRouter>
);
export default Routes;
