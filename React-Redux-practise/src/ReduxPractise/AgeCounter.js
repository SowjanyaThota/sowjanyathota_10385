import React, { Component } from 'react';
import { connect } from 'react-redux';
import './AgeCounter.css'

class Age extends Component {

    // state = {
    //     age: 21
    // }
    // onAgeUp = () => {
    //     this.setState({
    //         ...this.state,
    //         age : ++this.state.age
    //     })
    // }
    // onAgeDown = () => {
    //     this.setState({
    //         ...this.state,
    //         age : --this.state.age
    //     })
    // }
    render() {
        return (
            <div>
                <div align="center">
                    Age:<span>{this.props.age}</span><br />
                    <button onClick={this.props.OnAgeUp}>Age Up</button>
                    <button onClick={this.props.OnAgeDown}>Age Down</button>
                </div>
                <div>History</div>
                <div>
                    <ul>
                        {
                            this.props.history.map(el => (
                            <li className="History" key={el.id} onClick={() => this.props.OnDel(el.id)}>
                                {el.age}
                        </li>
                        )
                        )}
                    </ul>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    console.log(state);
    return {
        age: state.age,
        history:state.history
    }
}

const mapsDispachToProps = (dispach) => {
    return {
        OnAgeUp: () => dispach({ type: 'Age_Up', value: 1 }),
        OnAgeDown: () => dispach({ type: 'Age_Down', value: 1 }),
        OnDel : (id) => dispach({type:'DEL',key : id})
    }
}

export default connect(mapStateToProps, mapsDispachToProps)(Age);