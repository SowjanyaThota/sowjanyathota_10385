const MyMiddleware = (store) => {
    return next => {
        return action => {
            const result = next(action);
            console.log(`${JSON.stringify(result)}`)
        }
    }
}

export default MyMiddleware