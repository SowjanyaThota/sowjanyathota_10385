import React, { Component } from 'react';
import {connect} from 'react-redux';

class Multiple extends Component {
    render() {
        return (
            <div className="mul">
                <div className="col">
                    <div><span>A:</span><span>{this.props.a}</span></div>
                    <button onClick={() =>this.props.UpdateA(this.props.b)}>UpdateA</button>

                </div>
                <div className="col">
                    <div><span>B:</span><span>{this.props.b}</span></div>
                    <button onClick={() =>this.props.UpdateB(this.props.a)}>UpdateB</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        a: state.ReducerA.a,
        b: state.ReducerB.b
    }
}

const mapDispachToProps = (dispach) => {
    return {
        UpdateA : (b) => dispach({type:'UPDATE_A',b:b}),
        UpdateB : (a) => dispach({type:'UPDATE_B',a:a})
    }
}

export default connect(mapStateToProps,mapDispachToProps)(Multiple)

