import React, { Component } from 'react';

class DisplayEmployees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayEmployee')
            .then(res => res.json())
            .then(employeeData => {
                this.setState({
                    isLoaded: true,
                    emplopyees: employeeData
                })
            });
    }
    render() {
        var { isLoaded, employees } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div>
                
                <ul>
                    {employees.map(employee => (
                        <li key="{employee.id}">
                            Name: {employees.employeeId} | 
                            Email: {employees.employeeName} |
                            Role : {employees.role} |
                            Favourite Food : {employees.securityQuestion} |

                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}
export default DisplayEmployees;