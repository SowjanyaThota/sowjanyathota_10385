import React, { Component } from 'react';
import './App.css';
import Validation from '../src/validationform'
import Validate from '../src/validate'
import Counter from '../src/Component/counter'
import Counters from '../src/Component/counters'
import Learn from '../src/Component/learnBootstrap'
import NavBar from '../src/Component/navBar'
class App extends Component {

  constructor() {
    super();
    console.log("app-constructor")
  }
  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };

  componentDidMount() {
    console.log("mounted")
  }

  handleReset = () => {
    const counters = this.state.counters.map(c => {
      c.value = 0;
      return c;
    })
    this.setState({ counters })
  }

  handleIncrement = counter => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters })
  }
  handleDelete = counterId => {
    console.log("event hanlder called", counterId)
    const counters = this.state.counters.filter(c => c.id !== counterId)
    this.setState({ counters })
  };
  render() {
    console.log("app-rendered")
    return (

      <React.Fragment>
        {/* <Validation /> */}
        {/* <Validate /> */}
        {/* <Counter /> */}
        <NavBar totalCounters={this.state.counters.filter(c => c.value > 0).length} />
        <div>
          <Counters counters={this.state.counters} onReset={this.handleReset} onDelete={this.handleDelete} onIncrement={this.handleIncrement} />
        </div>
        {/* <Learn /> */}
      </React.Fragment>
    );
  }
}

export default App;
