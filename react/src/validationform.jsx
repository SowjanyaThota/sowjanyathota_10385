import React, { Component } from 'react';


class Validation extends Component {

    // render() {
    //     function handleEnter(event) {
    //         if (event.keyCode === 13) {
    //             const form = event.target.form;
    //             const index = Array.prototype.indexOf.call(form, event.target);
    //             form.elements[index + 1].focus();
    //             event.preventDefault();
    //         }
    //     }
    //     return (
    //         <form>
    //             <fieldset>
    //             <input type="text" ref='name' minLength="5" maxLength="10" required placeholder="Enter Name" onKeyDown={handleEnter}/><br /><br />
    //             <input type="text" ref='age' placeholder="Enter age" required min="18" max="28" onKeyDown={handleEnter} /><br /><br />
    //             <textarea  ref='meassage' minlength="3" maxlength="50" rows="6" cols="22"  placeholder="message" onKeyDown={handleEnter} /><br /><br />
    //             <button type="button" class="btn btn-info" >Submit</button>
    //             </fieldset>
    //         </form>
    //     );
    // }
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            phoneNumber: "",
            nameError: "",
            emailError: "",
            passwordError: "",
            phoneNumError: ""

        }
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeName = this.onChangeName.bind(this)
        this.onChangePhone = this.onChangePhone.bind(this)
    }



    onChangeName(e) {
        this.setState({ name: e.target.value }, () => {
            this.validateName()
        }
        )
    }
    onChangePhone(e) {
        this.setState({phoneNumber:e.target.value},
            () => { this.validatePhone()})
    }

    onChangeEmail(e) {
        debugger;
        this.setState({
            email: e.target.value
        }, () => { this.validateEmail() })
    }

    validateName() {
        const { name } = this.state
        this.setState({
			nameError:name.length > 3 && name.length < 40 &&((/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/).test(name)||(/^[a-zA-Z]*$/).test(name))? null : 'Length should be in the range of 3 to 25'})
    }

    validatePhone() {
        const { phoneNumber } = this.state
        this.setState({
            phoneNumError: 
          (/^[6-9]{1}[0-9]{9}$/ ).test(phoneNumber) ? null : 'Invalid Mobile Number',

        }) 
    }


    validateEmail() {
        debugger;
        const { email } = this.state
        console.log(this.state.email);
        this.setState({
            emailError: email.match('@') ? null : "not valid",
        })

        return true;
    }
    // handleChange1(e) {
    //     this.setState({ email: e.target.value })
    // }
    // handleChange2(e) {
    //     this.setState({ password: e.target.value })
    // }

    // handleSubmit(e) {
    //     debugger;
    //     e.preventDefault();
    //     const isValid = this.validate();
    //     if (isValid) {
    //         console.log(this.state);
    //     }
    // }


    render() {
        function handleEnter(event) {
                    if (event.keyCode === 13) {
                        const form = event.target.form;
                        const index = Array.prototype.indexOf.call(form, event.target);
                        form.elements[index + 1].focus();
                        event.preventDefault();
                    }
                }
        return (
            <div>
                <form align="center" >
                    <div>

                        <input type="text" name="name" placeholder="Name" onChange={this.onChangeName} onKeyDown={handleEnter} value={this.state.name} />
                        <div style={{ fontSize: 12, color: "red" }}>{this.state.nameError}</div>
                    </div>
                    <br />
                    <div>
                        <input type="text" name="email" placeholder="email" onChange={this.onChangeEmail} onKeyDown={handleEnter} value={this.state.email} />
                        <div style={{ fontSize: 12, color: "red" }}>{this.state.emailError}</div>
                    </div>
                    <br />
                    <div>
                        <input type="password" name="password" placeholder="password" onChange={this.handleChange} onKeyDown={handleEnter} value={this.state.password} />
                        <div style={{ fontSize: 12, color: "red" }}>{this.state.passwordError}</div>
                    </div>
                    <br />
                    <div>
                        <input type="text" name="phoneNumber" placeholder="phoneNumber" onChange={this.onChangePhone} onKeyDown={this.handleEnter} value={this.state.phoneNumber} />
                        <div style={{ fontSize: 12, color: "red" }}>{this.state.passwordError}</div>
                    </div>
                    <br />
                    <div>
                        <button type="submit" >submit</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Validation; 