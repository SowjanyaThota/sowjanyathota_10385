// npm dependencies
import React from 'react';
import ReactDOM from 'react-dom';

// project configuration
import * as serviceWorker from './serviceWorker';

// component dependencies
import App from './App';

// style dependencies

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import 'bootstrap/dist/css/bootstrap.css'
// import configureStore from './store/utilities/configureStore'

// const storeInstance = configureStore();

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
