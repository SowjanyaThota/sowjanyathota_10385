import React, { Component } from 'react';

class Counter extends Component {
    // handleIncrement = () => {
       
    //     this.setState({
    //         value: this.state.value + 1
    //     })
    //     console.log(this.state.value)

    // }
// componentDidUpdate(prevProps,prevState) {
// console.log('prevProps',prevProps);
// console.log('prevState',prevState)
// }
componentWillUnmount() {
    console.log("unmounteed")
}
    

    render() {
        console.log(this.props);
        console.log("counter rendered")
        return (
            <div>
              
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button
                    onClick={() => this.props.onIncrement(this.props.counter)}
                    className="btn btn-secondary btn-sm">Increment</button>
                    <button className="btn btn-danger m-2 btn-sm" onClick={() => this.props.onDelete(this.props.counter.id)}>Delete</button>
            </div>
        );
    }
    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += this.props.value === 0 ? "warning" : "primary";
        return classes;
    }

    /**
     * formatcount is a method to display zero string if count is zero if not count value
     */
    formatCount() {
        /**
         * object distructer means assigning state to a const and using that 
         */
        const { value: count } = this.props.counter;
        return count == 0 ? "Zero" : count
    }
}

export default Counter;
