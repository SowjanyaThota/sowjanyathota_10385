import React, { Component } from 'react';


const NavBar = ({totalCounters}) => {
    console.log("navbar")
    return (
        <nav className="navbar-light bg-light">
            <a className="navabr-brand" href="#">
                Navbar{" "}
                <span className="badge-secondary badge-pills">
                    {totalCounters}
                </span></a>
        </nav>
    );
}

// class NavBar extends Component {

//     render() {
//         return (
//             <nav className="navbar-light bg-light">
//                 <a className="navabr-brand" href="#">Navbar <badge className="badge-secondary badge-pills">{this.props.totalCounters}</badge></a>
//             </nav>
//         );
//     }
// }

export default NavBar;