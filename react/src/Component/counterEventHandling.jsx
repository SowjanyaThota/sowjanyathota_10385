import React, { Component } from 'react';

class Counter extends Component {
    state = {
        count: 0,
        // imageUrl:"http://picsum.photos/200"
        // address : {
        //     street:''
        // }
    };
    constructor() {
        super();
        this.state = {
            count: 0,
            // imageUrl:"http://picsum.photos/200"
            // address : {
            //     street:''
            // }
        };
        // this.handleIncrement = this.handleIncrement.bind(this)
    }


    //    styles = {
    //        fontSize : 10,
    //        fontWeight : 'bold'
    //    }


    // handleIncrement() {
    //     console.log("increment clicked",this.state.count);
    // }

    handleIncrement = () => {
             
              this.setState({
                  count : this.state.count + 1
              })
              console.log(this.state.count)

    }
    render() {
        let classes = this.getBadgeClasses();
        return (
            <div>
                {/* <img src={this.state.imageUrl} alt="" /> */}
                {/* badge is a bootstrap class and m-2 is margin 2 */}
                <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
                <button
                    onClick={this.handleIncrement}
                    className="btn btn-secondary btn-sm">Increment</button>
            </div>
        );
    }
    getBadgeClasses() {
        let classes = "badge m-2 badge-";
        classes += this.state.count === 0 ? "warning" : "primary";
        return classes;
    }

    /**
     * formatcount is a method to display zero string if count is zero if not count value
     */
    formatCount() {
        /**
         * object distructer means assigning state to a const and using that 
         */
        const { count } = this.state;
        return count == 0 ? "Zero" : count
    }
}

export default Counter;
