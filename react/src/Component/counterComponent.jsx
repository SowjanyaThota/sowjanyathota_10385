import React, { Component } from 'react';
/**
 * conditional rendering
 * writing all the sattemets required in one method and using that methos through states
 */
class CounterComponent extends Component {
    state = {
        count :0,
        tag : []
      }

      renderTags() {
          if(this.state.tag.length === 0) return <p>there are no tags</p>
          return  <ul> {this.state.tag.map(tag => <li key={tag}>{tag}</li>)} </ul>
      }
    render() { 
        return ( 
            <div>  
                {(this.state.tag.length === 0) && "no tags"}
                    {this.renderTags()}             
            </div>
         );
    }
}
 
export default CounterComponent;