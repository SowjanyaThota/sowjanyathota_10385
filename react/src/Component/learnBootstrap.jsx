import React, { Component } from 'react';

class Learn extends Component {
    state = {
        
    }

    render() {
        let newCard = [
            { head: "Light Card", title: "Light card title", text: "Some quick example text to build on the card title and make up the bulk of the card's content." },
            { head: "Dark Card", title: "Dark card title", text: "Some quick example text to build on the card title and make up the bulk of the card's content." },
            { head: "Red Card", title: "red card title", text: "Some quick example text to build on the card title and make up the bulk of the card's content." }
        ]
        var fun = newCard.map(newfun =>
            <div className ={this.classses} style={{ maxWidth: "18rem" }}>
                <div class="card-header">{newfun.head}</div>
                <div class="card-body">
                    <h5 class="card-title">{newfun.title}</h5>
                    <p class="card-text">{newfun.text}</p>
                </div>
            </div>
        )
        let classses = "card text-white bg-info mb-3"

        return (
            <div>{fun}</div>
        );

    }
}

export default Learn;