import * as allActions from '../actions/actionConstants';

const initialise = {
    is_logged_in : false
}

export default function authReducer(state = initialise,action) {
    switch(action.type) {
        case allActions.LOGIN_USER_SUCCESS:
        return  {
            ...state,
            is_logged_in :true
           }

        default :
        return state
    }
}