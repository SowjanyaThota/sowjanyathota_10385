import {createStore,applyMiddleware,compose} from 'redux';
import rootReducer from '../reducer/rootReducer';
import newsMiddleware from '../middlewares/newsService';
import authMiddleware from '../middlewares/authService';

export default function configureStore() {
return createStore(
    rootReducer,
   compose(applyMiddleware(
       newsMiddleware,
       authMiddleware
   ))
);
}