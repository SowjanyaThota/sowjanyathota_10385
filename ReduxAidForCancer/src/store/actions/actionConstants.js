export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';



export const FETCH_NEWS = "FETCH_NEWS";
export const RECEIVE_NEWS = "RECEIVE_NEWS";



export const Post_News = "POST_NEWS";



export const FETCH_NEWS_BY_ID = "FETCH_NEWS_BY_ID";
export const RECEIVE_NEWS_BY_ID = "RECEIVE_NEWS_BY_ID";


export const DO_LOGIN_USER = "DO_LOGIN_USER";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";


export const LOGOUT_USER = "LOGOUT_USER"