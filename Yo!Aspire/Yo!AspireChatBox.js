 // JQuery Function to display the hiding chat window when we 
            // click on respective anchor tag
            $("#nav a").click(function (e) {
                e.preventDefault();
                $(".toggle").hide();
                var toShow = $(this).attr('href');
                $(toShow).show();
            });
            // Function for the first contact to add the list tag automatically to the 
            // chat body when user sends a message
            function mytext1() {
                var date=new Date();
                var hour=date.getHours();
                var min=date.getMinutes();
                var ampm=hour>=12?'pm':'am';
                hour = hour%12;
                hour = hour?hour:12;
                hour = hour<10?'0'+hour:hour;
                min = min<10?'0'+min:min;

                let checkText = document.getElementById("text1").value;
                document.getElementById("p1").innerHTML = document.getElementById("text1").value+" "+hour+":"+min+ampm;
                if(checkText != ""){
                    var node = document.createElement("LI");
                    node.className = "chat-list";
                    var textnode = document.createTextNode(document.getElementById("text1").value+" "+hour+":"+min+ampm);
                    node.appendChild(textnode);
                    document.getElementById("myList1").appendChild(node);
                }
                document.getElementById("text1").value = "";
                document.getElementById("text1").focus();
            }
            // Function for the second contact to add the list tag automatically to the 
            // chat body when user sends a message
            function mytext2() {
                let checkTest = document.getElementById("text2").value;
                document.getElementById("p2").innerHTML = document.getElementById("text2").value;
                if(checkTest != "") {
                    var node = document.createElement("LI");
                    node.className = "chat-list";
                    var textnode = document.createTextNode(document.getElementById("text2").value);
                    node.appendChild(textnode);
                    document.getElementById("myList2").appendChild(node);
                }
                document.getElementById("text2").value = "";
                document.getElementById("text2").focus();
            }
            // Function for the third contact to add the list tag automatically to the 
            // chat body when user sends a message
            function mytext3() {
                let checkTest = document.getElementById("text3").value;
                document.getElementById("p3").innerHTML = document.getElementById("text3").value;
                if(checkTest != "") {
                    var node = document.createElement("LI");
                    node.className = "chat-list";
                    var textnode = document.createTextNode(document.getElementById("text3").value);
                    node.appendChild(textnode);
                    document.getElementById("myList3").appendChild(node);
                }
                document.getElementById("text3").value = "";
                document.getElementById("text3").focus();
            }
            // Function for the fourth contact to add the list tag automatically to the 
            // chat body when user sends a message 
            function mytext4() {
                let checkTest = document.getElementById("text4").value;
                document.getElementById("p4").innerHTML = document.getElementById("text4").value;
                if(checkTest != "") {
                    var node = document.createElement("LI");
                    node.className = "chat-list";
                    var textnode = document.createTextNode(document.getElementById("text4").value);
                    node.appendChild(textnode);
                    document.getElementById("myList4").appendChild(node);
                }
                document.getElementById("text4").value = "";
                document.getElementById("text4").focus();
            }
            // Function for the fifth contact to add the list tag automatically to the 
            // chat body when user sends a message 
            function mytext5() {
                let checkTest = document.getElementById("text5").value;
                document.getElementById("p5").innerHTML = document.getElementById("text5").value;
                if(checkTest != "") {
                    var node = document.createElement("LI");
                    node.className = "chat-list";
                    var textnode = document.createTextNode(document.getElementById("text5").value);
                    node.appendChild(textnode);
                    document.getElementById("myList5").appendChild(node);
                }
                document.getElementById("text5").value = "";
                document.getElementById("text5").focus();
            }
           
        $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#nav *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });